#ifndef ABSTRACTVIEW_H
#define ABSTRACTVIEW_H

#include "Global.h"

class AbstractView
{
public:
    virtual ~AbstractView() = default;

    virtual void notify(Event event) = 0;
};

#endif // ABSTRACTVIEW_H
