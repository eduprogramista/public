#ifndef GAMEOFLIFE_H
#define GAMEOFLIFE_H

#include "Global.h"
#include <vector>
#include <thread>
#include <mutex>
#include <atomic>

class AbstractView;

class GameOfLife
{
    using Mutex = std::recursive_mutex;
public:
    GameOfLife(int _w, int _h);
    ~GameOfLife();

    void random();
    void setSpeed(int _speed);
    int getSpeed() const;
    bool evolution();

    void start();
    void stop();

    void attachView(AbstractView* _view);
    void detachView(AbstractView* _view);

    int getWidth() const;
    int getHeight() const;

    std::vector<Cell> getAllCells() const;
    void setCell(int i, int j, Cell state);

protected:
    void notifyViews(Event event) const;

private:
    int w, h;
    Cell* tab;
    std::vector<AbstractView*> views;
    int speed;
    std::thread worker;
    mutable Mutex this_mutex;
    std::atomic<bool> flag;
};

#endif // GAMEOFLIFE_H
