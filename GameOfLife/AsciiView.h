#ifndef ASCIIVIEW_H
#define ASCIIVIEW_H

#include "AbstractView.h"

class GameOfLife;

class AsciiView : public AbstractView
{
public:
    AsciiView(GameOfLife* _game);
    ~AsciiView();

    void notify(Event event) override;

private:
    GameOfLife* game;
};

#endif // ASCIIVIEW_H
