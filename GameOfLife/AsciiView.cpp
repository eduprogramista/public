#include "AsciiView.h"
#include "GameOfLife.h"
#include <iostream>
#include <cstdlib>

using namespace std;

AsciiView::AsciiView(GameOfLife *_game) : game(_game)
{
    game->attachView(this);
    notify(CHANGED);
}

AsciiView::~AsciiView()
{
    game->stop();
    game->detachView(this);
}

void AsciiView::notify(Event event)
{
    if(event == CHANGED)
    {
        int w = game->getWidth();
        int h = game->getHeight();

        std::vector<Cell> tab = game->getAllCells();

        for(int i = 0; i < w; ++i)
            cout << "+---";
        cout << "+\n";

        for(int i = 0; i < h; ++i)
        {
            for(int j = 0; j < w; ++j)
            {
                cout << "|" << (tab[i * w + j] == DEAD ? "   " : "###");
            }
            cout << "|\n";

            for(int i = 0; i < w; ++i)
                cout << "+---";
            cout << "+\n";
        }
        cout << endl;
    }
    else if(event == STARTED)
    {
        cout << "GAME PLAYS" << endl;
    }
    else if(event == FINISHED || event == TERMINATED)
    {
        cout << "GAME OVER" << endl;
    }
}
