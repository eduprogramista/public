#ifndef GUIVIEW_H
#define GUIVIEW_H

#include "AbstractView.h"
#include <QWidget>
#include <vector>
#include <thread>
#include <atomic>

class GameOfLife;
class QGraphicsView;
class QGraphicsScene;
class QAbstractGraphicsShapeItem;
class QPushButton;
class QComboBox;

class GuiView : public QWidget, public AbstractView
{
    Q_OBJECT
public:
    GuiView(GameOfLife* _game, int _viewWidth = 280, int _viewHeight = 300, QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
    ~GuiView();

    void notify(Event event) override;

signals:
    void gameChanged();
    void gameStarted();
    void gameFinished();

private slots:
    void changedSlot();
    void startedSlot();
    void finishedSlot();

protected:
    void makeScene();
    void makeWidgets();
    void makeLayout();
    void makeSignalsSlotsConnection();

private:
    int viewWidth;
    int viewHeight;
    QGraphicsView* view;
    QGraphicsScene* scene;
    std::vector<QAbstractGraphicsShapeItem*> shapes;

    QPushButton* startStopButton;
    QPushButton* randomButton;
    QComboBox* speedBox;

    GameOfLife* game;
};

#endif // GUIVIEW_H
