#-------------------------------------------------
#
# Project created by QtCreator 2018-05-30T12:58:57
#
#-------------------------------------------------

QT       += core gui
CONFIG   += c++14

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GameOfLife
TEMPLATE = app

SOURCES += main.cpp\
    AsciiView.cpp \
    GameOfLife.cpp \
    GuiView.cpp

HEADERS  += \
    AbstractView.h \
    AsciiView.h \
    GameOfLife.h \
    GuiView.h \
    Global.h
