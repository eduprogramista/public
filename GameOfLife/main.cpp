#include <QApplication>
#include "GuiView.h"
#include "AsciiView.h"
#include "GameOfLife.h"
#include <thread>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    GameOfLife game(15, 15);
    GuiView guiView(&game);
    AsciiView asciiView(&game);

    return a.exec();
}
