#include "GameOfLife.h"
#include "AbstractView.h"
#include <cstdlib>

GameOfLife::GameOfLife(int _w, int _h) : w(_w), h(_h), speed(2), flag(false)
{
    tab = new Cell[w * h];
    random();
}

GameOfLife::~GameOfLife()
{
    stop();
    delete[] tab;
}

void GameOfLife::random()
{
    std::lock_guard<Mutex> guard(this_mutex);
    for(int i = 0; i < w * h; ++i)
        tab[i] = (rand() % 2 == 0) ? DEAD : ALIVE;

    notifyViews(CHANGED);
}

void GameOfLife::setSpeed(int _speed)
{
    std::lock_guard<Mutex> guard(this_mutex);
    speed = _speed;
}

int GameOfLife::getSpeed() const
{
    std::lock_guard<Mutex> guard(this_mutex);
    return speed;
}

bool GameOfLife::evolution()
{
    bool alive = false;
    Cell* nextTab = new Cell[w * h];
    for(int i = 0; i < h; i++)
    {
        for(int j = 0; j < w; ++j)
        {
            int counter = 0;

            if(j + 1 < w && tab[i * w + (j + 1)] == ALIVE)
                counter++;
            if(j + 1 < w && i + 1 < h && tab[(i + 1) * w + (j + 1)] == ALIVE)
                counter++;
            if(i + 1 < h && tab[(i + 1) * w + j] == ALIVE)
                counter++;
            if(i + 1 < h && j - 1 >= 0 && tab[(i + 1) * w + (j - 1)] == ALIVE)
                counter++;
            if(j - 1 >= 0 && tab[i * w + (j - 1)] == ALIVE)
                counter++;
            if(i - 1 >= 0 && j - 1 >= 0 && tab[(i - 1) * w + (j - 1)] == ALIVE)
                counter++;
            if(i - 1 >= 0 && tab[(i - 1) * w + j] == ALIVE)
                counter++;
            if(i - 1 >= 0 && j + 1 < w && tab[(i - 1) * w + (j + 1)] == ALIVE)
                counter++;

            if(counter == 2)
                nextTab[i * w + j] = tab[i * w + j];
            else if(counter == 3)
                nextTab[i * w + j] = ALIVE;
            else
                nextTab[i * w + j] = DEAD;

            if(nextTab[i * w + j] != tab[i * w + j])
                alive = true;
        }
    }

    std::lock_guard<Mutex> guard(this_mutex);
    delete[] tab;
    tab = nextTab;

    notifyViews(alive ? CHANGED : FINISHED);

    return alive;
}

void GameOfLife::start()
{
    auto workerTask = [=]{
        flag = true;
        do {
            flag = evolution();
            for(int i = 0; i < speed; ++i)
            {
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
                if(!flag) break;
            }
        } while(flag);
    };

    bool started = !flag;
    stop();

    if(started)
        notifyViews(STARTED);

    worker = std::thread(workerTask);
}

void GameOfLife::stop()
{
    bool terminated = flag;
    flag = false;
    if(worker.joinable())
        worker.join();

    if(terminated)
        notifyViews(TERMINATED);
}

void GameOfLife::attachView(AbstractView* _view)
{
    std::lock_guard<Mutex> guard(this_mutex);
    views.push_back(_view);
}

void GameOfLife::detachView(AbstractView* _view)
{
    std::lock_guard<Mutex> guard(this_mutex);
    views.erase(std::remove(views.begin(), views.end(), _view));
}

int GameOfLife::getWidth() const
{
    return w;
}

int GameOfLife::getHeight() const
{
    return h;
}

std::vector<Cell> GameOfLife::getAllCells() const
{
    std::lock_guard<Mutex> guard(this_mutex);
    return std::vector<Cell>(tab, tab + w * h);
}

void GameOfLife::setCell(int i, int j, Cell state)
{
    std::lock_guard<Mutex> guard(this_mutex);
    tab[i * w + j] = state;
}

void GameOfLife::notifyViews(Event event) const
{
    std::lock_guard<Mutex> guard(this_mutex);
    for(auto view : views)
        view->notify(event);
}
