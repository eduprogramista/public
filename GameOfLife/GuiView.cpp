#include "GuiView.h"
#include "GameOfLife.h"
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGridLayout>
#include <QGraphicsRectItem>
#include <QPushButton>
#include <QComboBox>

GuiView::GuiView(GameOfLife* _game, int _viewWidth, int _viewHeight, QWidget *parent, Qt::WindowFlags f)
    : QWidget(parent, f), viewWidth(_viewWidth), viewHeight(_viewHeight)
{
    game = _game;
    game->attachView(this);

    makeScene();
    makeWidgets();
    makeLayout();
    makeSignalsSlotsConnection();

    setFixedSize(viewWidth + 40, viewHeight + 80);
    show();
}

GuiView::~GuiView()
{
    game->stop();
    game->detachView(this);
}

void GuiView::notify(Event event)
{
    if(event == CHANGED)
        emit gameChanged();
    else if(event == STARTED)
        emit gameStarted();
    else if(event == FINISHED || event == TERMINATED)
        emit gameFinished();
}

void GuiView::changedSlot()
{
    std::vector<Cell> tab = game->getAllCells();
    int w = game->getWidth();
    int h = game->getHeight();
    for(int i = 0; i < h; ++i)
    {
        for(int j = 0; j < w; ++j)
        {
            if(tab[i * w + j] == ALIVE)
                shapes[i * w + j]->setBrush(Qt::green);
            else
                shapes[i * w + j]->setBrush(Qt::white);
        }
    }
}

void GuiView::startedSlot()
{
    startStopButton->setText("Stop");
    randomButton->setEnabled(false);
}

void GuiView::finishedSlot()
{
    startStopButton->setText("Start");
    randomButton->setEnabled(true);
}

void GuiView::makeScene()
{
    scene = new QGraphicsScene;
    view = new QGraphicsView(scene);
    view->setFixedSize(viewWidth, viewHeight);

    std::vector<Cell> tab = game->getAllCells();
    int w = game->getWidth();
    int h = game->getHeight();
    for(int i = 0; i < h; ++i)
    {
        for(int j = 0; j < w; ++j)
        {
            QAbstractGraphicsShapeItem* shape = new QGraphicsRectItem(0, 0, viewWidth / w - 4, viewHeight / h - 4);
            shapes.push_back(shape);
            scene->addItem(shape);
            shape->setPos(j * viewWidth / w - 4, i * viewHeight / h - 4);
        }
    }
    changedSlot();
}

void GuiView::makeWidgets()
{
    randomButton = new QPushButton("Losuj");
    startStopButton = new QPushButton("Start");

    QStringList speedItems;
    for(int i = 1; i <= 5; ++i)
        speedItems.append(QString::number(i));
    speedBox = new QComboBox;
    speedBox->addItems(speedItems);
    int speed = game->getSpeed();
    speedBox->setCurrentText(QString::number(speed));
}

void GuiView::makeLayout()
{
    QGridLayout* layout = new QGridLayout;
    layout->addWidget(view, 0, 0, 10, 10);
    layout->addWidget(randomButton, 10, 0, 1, 3);
    layout->addWidget(speedBox, 10, 5, 1, 2);
    layout->addWidget(startStopButton, 10, 7, 1, 3);
    setLayout(layout);
}

void GuiView::makeSignalsSlotsConnection()
{
    QObject::connect(randomButton, &QPushButton::clicked, [=]{ game->random(); });
    QObject::connect(speedBox, &QComboBox::currentTextChanged, [=]{ game->setSpeed(speedBox->currentText().toInt()); });
    QObject::connect(startStopButton, &QPushButton::clicked, [=]{
        if(startStopButton->text() == "Start") game->start();
        else game->stop();
    });

    QObject::connect(this, &GuiView::gameChanged, this, &GuiView::changedSlot, Qt::QueuedConnection);
    QObject::connect(this, &GuiView::gameStarted, this, &GuiView::startedSlot, Qt::QueuedConnection);
    QObject::connect(this, &GuiView::gameFinished, this, &GuiView::finishedSlot, Qt::QueuedConnection);
}
