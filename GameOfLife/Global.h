#ifndef GLOBAL_H
#define GLOBAL_H

enum Cell
{
    DEAD,
    ALIVE
};

enum Event
{
    CHANGED,
    STARTED,
    FINISHED,
    TERMINATED
};

#endif // GLOBAL_H
