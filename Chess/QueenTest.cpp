#include "QueenTest.h"
#include "GameBoard.h"
#include "Queen.h"
#include "ChessmanFactory.h"
#include <iostream>

QueenTest::QueenTest()
{
    testCase0();
    testCase1();
    testCase2();
    testCase3();
    testCase4();
    testCase5();
    testCase6();
    testCase7();
    testCase8();
    testCase9();
    testCase10();
    testCase11();
    testCase12();
    testCase13();
    testCase14();
    testCase15();
    testCase16();
    testCase17();
    testCase18();
    testCase19();
    testCase20();
    testCase21();
    testCase22();
    testCase23();
    testCase24();
    testCase25();
    testCase26();
    testCase27();
    testCase28();
    testCase29();
    testCase30();
    testCase31();
    testCase32();
}

// diagonal moveActions

void QueenTest::testCase0()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    assert(queen->moveActions(Position(6, 6)).empty() == false);
}

void QueenTest::testCase1()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    assert(queen->moveActions(Position(1, 1)).empty() == false);
}

void QueenTest::testCase2()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    assert(queen->moveActions(Position(1, 5)).empty() == false);
}

void QueenTest::testCase3()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    assert(queen->moveActions(Position(5, 1)).empty() == false);
}

// diagonal occupied

void QueenTest::testCase4()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    Pawn* pawn = blackFactory.createPawn(6, 6);
    assert(queen->moveActions(Position(7, 7)).empty() == true);
}

void QueenTest::testCase5()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    Pawn* pawn = blackFactory.createPawn(1, 1);
    assert(queen->moveActions(Position(0, 0)).empty() == true);
}

void QueenTest::testCase6()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    Pawn* pawn = blackFactory.createPawn(1, 5);
    assert(queen->moveActions(Position(0, 6)).empty() == true);
}

void QueenTest::testCase7()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    Pawn* pawn = blackFactory.createPawn(5, 1);
    assert(queen->moveActions(Position(6, 0)).empty() == true);
}

// diagonal attack same color

void QueenTest::testCase8()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    Pawn* pawn = blackFactory.createPawn(7, 7);
    assert(queen->moveActions(Position(7, 7)).empty() == true);
}

void QueenTest::testCase9()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    Pawn* pawn = blackFactory.createPawn(0, 0);
    assert(queen->moveActions(Position(0, 0)).empty() == true);
}

void QueenTest::testCase10()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    Pawn* pawn = blackFactory.createPawn(0, 6);
    assert(queen->moveActions(Position(0, 6)).empty() == true);
}

void QueenTest::testCase11()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    Pawn* pawn = blackFactory.createPawn(6, 0);
    assert(queen->moveActions(Position(6, 0)).empty() == true);
}

// diagonal attack different color

void QueenTest::testCase12()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);
    ChessmanFactory whiteFactory(&gameBoard, Color::WHITE);

    Queen* queen = blackFactory.createQueen(3, 3);
    Pawn* pawn = whiteFactory.createPawn(7, 7);
    assert(queen->moveActions(Position(7, 7)).empty() == false);
}

void QueenTest::testCase13()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);
    ChessmanFactory whiteFactory(&gameBoard, Color::WHITE);

    Queen* queen = blackFactory.createQueen(3, 3);
    Pawn* pawn = whiteFactory.createPawn(0, 0);
    assert(queen->moveActions(Position(0, 0)).empty() == false);
}

void QueenTest::testCase14()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);
    ChessmanFactory whiteFactory(&gameBoard, Color::WHITE);

    Queen* queen = blackFactory.createQueen(3, 3);
    Pawn* pawn = whiteFactory.createPawn(0, 6);
    assert(queen->moveActions(Position(0, 6)).empty() == false);
}

void QueenTest::testCase15()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);
    ChessmanFactory whiteFactory(&gameBoard, Color::WHITE);

    Queen* queen = blackFactory.createQueen(3, 3);
    Pawn* pawn = whiteFactory.createPawn(6, 0);
    assert(queen->moveActions(Position(6, 0)).empty() == false);
}

// straight

void QueenTest::testCase16()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    assert(queen->moveActions(Position(3, 5)).empty() == false);
}

void QueenTest::testCase17()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    assert(queen->moveActions(Position(5, 3)).empty() == false);
}

void QueenTest::testCase18()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    assert(queen->moveActions(Position(1, 3)).empty() == false);
}

void QueenTest::testCase19()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    assert(queen->moveActions(Position(3, 1)).empty() == false);
}

// straight occupied

void QueenTest::testCase20()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    Pawn* pawn = blackFactory.createPawn(3, 5);
    assert(queen->moveActions(Position(3, 6)).empty() == true);
}

void QueenTest::testCase21()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    Pawn* pawn = blackFactory.createPawn(5, 3);
    assert(queen->moveActions(Position(6, 3)).empty() == true);
}

void QueenTest::testCase22()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    Pawn* pawn = blackFactory.createPawn(1, 3);
    assert(queen->moveActions(Position(0, 3)).empty() == true);
}

void QueenTest::testCase23()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    Pawn* pawn = blackFactory.createPawn(3, 1);
    assert(queen->moveActions(Position(3, 0)).empty() == true);
}

// straight attack same color

void QueenTest::testCase24()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    Pawn* pawn = blackFactory.createPawn(3, 6);
    assert(queen->moveActions(Position(3, 6)).empty() == true);
}

void QueenTest::testCase25()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    Pawn* pawn = blackFactory.createPawn(6, 3);
    assert(queen->moveActions(Position(6, 3)).empty() == true);
}

void QueenTest::testCase26()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    Pawn* pawn = blackFactory.createPawn(0, 3);
    assert(queen->moveActions(Position(0, 3)).empty() == true);
}

void QueenTest::testCase27()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    Pawn* pawn = blackFactory.createPawn(3, 0);
    assert(queen->moveActions(Position(3, 0)).empty() == true);
}

// straight attack different color

void QueenTest::testCase28()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);
    ChessmanFactory whiteFactory(&gameBoard, Color::WHITE);

    Queen* queen = blackFactory.createQueen(3, 3);
    Pawn* pawn = whiteFactory.createPawn(3, 6);
    assert(queen->moveActions(Position(3, 6)).empty() == false);
}

void QueenTest::testCase29()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);
    ChessmanFactory whiteFactory(&gameBoard, Color::WHITE);

    Queen* queen = blackFactory.createQueen(3, 3);
    Pawn* pawn = whiteFactory.createPawn(6, 3);
    assert(queen->moveActions(Position(6, 3)).empty() == false);
}

void QueenTest::testCase30()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);
    ChessmanFactory whiteFactory(&gameBoard, Color::WHITE);

    Queen* queen = blackFactory.createQueen(3, 3);
    Pawn* pawn = whiteFactory.createPawn(0, 3);
    assert(queen->moveActions(Position(0, 3)).empty() == false);
}

void QueenTest::testCase31()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);
    ChessmanFactory whiteFactory(&gameBoard, Color::WHITE);

    Queen* queen = blackFactory.createQueen(3, 3);
    Pawn* pawn = whiteFactory.createPawn(3, 0);
    assert(queen->moveActions(Position(3, 0)).empty() == false);
}

// incorrect moveActions

void QueenTest::testCase32()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Queen* queen = blackFactory.createQueen(3, 3);
    assert(queen->moveActions(Position(5, 6)).empty() == true);
}
