#ifndef CHESSMAN_H
#define CHESSMAN_H

#include "GameBoard.h"
#include "Position.h"
#include "Color.h"
#include <ostream>
#include <queue>

using std::queue;

class Chessman
{
    friend std::ostream& operator<<(std::ostream& out, const Chessman& chessman);

public:
    Chessman(int _x, int _y, Color _color, char _symbol);
    virtual ~Chessman() = default;
    virtual Chessman* clone() const = 0;

    virtual queue<ActionInfo*> moveActions(Position _toPos) = 0;

    void setGameBoard(const GameBoard* _gameBoard);
    const GameBoard* getGameBoard() const;

    void setPosition(const Position& _pos);
    const Position& getPosition() const;

    Color getColor() const;

    void setFirstMove(bool _firstMove = false);
    bool isFirstMove() const;

protected:
    char symbol;
    const GameBoard* gameBoard;
    Position pos;
    Color color;
    bool firstMove;
};

#endif // CHESSMAN_H
