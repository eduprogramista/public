#ifndef ACTIONINFO_H
#define ACTIONINFO_H

#include "Position.h"
#include "ActionInfoBuilder.h"

class Chessman;

enum class ActionType
{
    REMOVE,
    MOVE,
    PROMOTION
};

class ActionInfo
{
public:
    ActionInfo(ActionType _actionType, Chessman* _chessman, const Position& _prevPosition, const Position& _nextPosition);

    ActionType getActionType() const;
    Chessman* getChessman() const;
    Position getPrevPosition() const;
    Position getNextPosition() const;

private:
    ActionType actionType;
    Chessman* chessman;
    Position prevPosition;
    Position nextPosition;
};

#endif // ACTIONINFO_H
