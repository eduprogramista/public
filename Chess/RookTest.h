#ifndef ROOKTEST_H
#define ROOKTEST_H


class RookTest
{
public:
    RookTest();

    // straight
    void testCase0();
    void testCase1();
    void testCase2();
    void testCase3();

    // straight occupied
    void testCase4();
    void testCase5();
    void testCase6();
    void testCase7();

    // straight attack same color
    void testCase8();
    void testCase9();
    void testCase10();
    void testCase11();

    // straight attack different color
    void testCase12();
    void testCase13();
    void testCase14();
    void testCase15();

    // incorrect move
    void testCase16();
    void testCase17();
};

#endif // ROOKTEST_H
