#ifndef GAMEBOARD_H
#define GAMEBOARD_H

#include "Global.h"
#include "ActionInfo.h"
#include <array>
#include <unordered_set>
#include <ostream>

class Chessman;

using std::unordered_set;
using std::unique_ptr;

class GameBoard
{
    using Row = std::array<Chessman*, Global::n>;
    using Board = std::array<Row, Global::n>;

    friend std::ostream& operator<<(std::ostream& out, const GameBoard& gameBoard);

public:
    GameBoard();
    GameBoard(const GameBoard& other);
    ~GameBoard();

    bool moveChessman(const Position& _fromPos, const Position& _toPos);

    Chessman* getChessman(const Position& _pos) const;
    void setChessman(const Position& _pos, Chessman* _chessman);
    void unsetChessman(Chessman* _chessman);
    void unsetAllChessmen();

    bool isCheck() const;
    bool isCheckmate() const;

    const ActionInfo* getLastActionInfo() const;
    void setLastActionInfo(const ActionInfo& _actionInfo);
    void undoLastAction();

    void addChessman(Chessman* _chessman);
    void removeChessman(Chessman* _chessman);
    void removeAllChessmen();

private:
    unordered_set<Chessman*> chessmen;
    Board board;
    bool check;
    bool checkmate;
    ActionInfo* lastActionInfo;
};

#endif // GAMEBOARD_H
