#ifndef BISHOP_H
#define BISHOP_H

#include "Chessman.h"

class Bishop : public Chessman
{
public:
    Bishop(int _x, int _y, Color _color);
    Bishop* clone() const override;

    queue<ActionInfo*> moveActions(Position _toPos) override;
};

#endif // BISHOP_H
