#include "Queen.h"
#include <cstdlib>

Queen::Queen(int _x, int _y, Color _color)
    : Chessman(_x, _y, _color, 'q')
{

}

Queen* Queen::clone() const
{
    return new Queen(*this);
}

queue<ActionInfo*> Queen::moveActions(Position _toPos)
{
    queue<ActionInfo*> actions;
    int dx = _toPos.x - pos.x;
    int dy = _toPos.y - pos.y;
    Chessman* attacked = gameBoard->getChessman(_toPos);

    bool horizontal = dx == 0 && abs(dy) != 0;
    bool vertical = abs(dx) != 0 && dy == 0;
    bool diagonal = dx != 0 && abs(dx) == abs(dy);
    bool horizontalMove = horizontal && !attacked;
    bool verticalMove = vertical && !attacked;
    bool diagonalMove = diagonal && !attacked;
    bool horizontalAttack = horizontal && attacked && attacked->getColor() != color;
    bool verticalAttack = vertical && attacked && attacked->getColor() != color;
    bool diagonalAttack = diagonal &&  attacked && attacked->getColor() != color;

    if(!horizontalMove && !verticalMove && !diagonalMove &&
            !horizontalAttack && !verticalAttack && !diagonalAttack)
        return actions;

    if(diagonalMove || diagonalAttack)
    {
        int sx = dx / abs(dx);
        int sy = dy / abs(dy);
        int i = pos.x + sx;
        int j = pos.y + sy;

        while(i != _toPos.x && j != _toPos.y)
        {
            if(gameBoard->getChessman(Position(i, j)))
                return actions;
            i += sx;
            j += sy;
        }
    }
    else if(horizontalMove || horizontalAttack)
    {
        int sy = dy / abs(dy);
        int i = pos.x;
        int j = pos.y + sy;

        while(j != _toPos.y)
        {
            if(gameBoard->getChessman(Position(i, j)))
                return actions;
            j += sy;
        }
    }
    else if(verticalMove || verticalAttack)
    {
        int sx = dx / abs(dx);
        int i = pos.x + sx;
        int j = pos.y;

        while(i != _toPos.x)
        {
            if(gameBoard->getChessman(Position(i, j)))
                return actions;
            i += sx;
        }
    }

    if(horizontalAttack || verticalAttack || diagonalAttack)
        actions.push(RemoveActionInfoBuilder::createActionInfo(attacked));

    actions.push(MoveActionInfoBuilder::createActionInfo(this, pos, _toPos));

    return actions;
}
