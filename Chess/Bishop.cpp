#include "Bishop.h"
#include "ActionInfo.h"
#include <cstdlib>

Bishop::Bishop(int _x, int _y, Color _color)
    : Chessman(_x, _y, _color, 'b')
{

}

Bishop* Bishop::clone() const
{
    return new Bishop(*this);
}

queue<ActionInfo*> Bishop::moveActions(Position _toPos)
{
    queue<ActionInfo*> actions;
    int dx = _toPos.x - pos.x;
    int dy = _toPos.y - pos.y;
    Chessman* attacked = gameBoard->getChessman(_toPos);

    bool diagonal = dx != 0 && abs(dx) == abs(dy);
    bool diagonalMove =  diagonal && !attacked;
    bool diagonalAttack = diagonal &&  attacked && attacked->getColor() != color;

    if(!diagonalMove && !diagonalAttack)
        return actions;

    int i = pos.x + -int(color) * -1;
    int j = pos.y + -int(color) * -1;
    while(i != _toPos.x && j != _toPos.y)
    {
        if(gameBoard->getChessman(Position(i, j)))
            return actions;

        i += -int(color) * -1;
        j += -int(color) * -1;
    }

    if(diagonalAttack)
        actions.push(RemoveActionInfoBuilder::createActionInfo(attacked));

    actions.push(MoveActionInfoBuilder::createActionInfo(this, pos, _toPos));

    return actions;
}
