#include "ActionInfoBuilder.h"
#include "ActionInfo.h"
#include "Chessman.h"

ActionInfo* RemoveActionInfoBuilder::createActionInfo(Chessman* _chessman)
{
    return new ActionInfo(ActionType::REMOVE, _chessman, Position(), Position());
}

ActionInfo* MoveActionInfoBuilder::createActionInfo(Chessman* _chessman, const Position& _prevPosition, const Position& _nextPosition)
{
    return new ActionInfo(ActionType::MOVE, _chessman, _prevPosition, _nextPosition);
}

ActionInfo* PromotionActionInfoBuilder::createActionInfo(Chessman* _chessman)
{
    return new ActionInfo(ActionType::PROMOTION, _chessman, _chessman->getPosition(), _chessman->getPosition());
}
