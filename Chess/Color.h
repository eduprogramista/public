#ifndef COLOR_H
#define COLOR_H

enum class Color
{
    WHITE = -1,
    BLACK = 1
};


#endif // COLOR_H
