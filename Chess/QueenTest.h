#ifndef QUEENTEST_H
#define QUEENTEST_H


class QueenTest
{
public:
    QueenTest();

    // diagonal move
    void testCase0();
    void testCase1();
    void testCase2();
    void testCase3();

    // diagonal occupied
    void testCase4();
    void testCase5();
    void testCase6();
    void testCase7();

    // diagonal attack same color
    void testCase8();
    void testCase9();
    void testCase10();
    void testCase11();

    // diagonal attack different color
    void testCase12();
    void testCase13();
    void testCase14();
    void testCase15();

    // straight
    void testCase16();
    void testCase17();
    void testCase18();
    void testCase19();

    // straight occupied
    void testCase20();
    void testCase21();
    void testCase22();
    void testCase23();

    // straight attack same color
    void testCase24();
    void testCase25();
    void testCase26();
    void testCase27();

    // straight attack different color
    void testCase28();
    void testCase29();
    void testCase30();
    void testCase31();

    // incorrect move
    void testCase32();
};

#endif // QUEENTEST_H
