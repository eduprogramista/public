#ifndef ROOK_H
#define ROOK_H

#include "Chessman.h"

class Rook : public Chessman
{
public:
    Rook(int _x, int _y, Color _color);
    Rook* clone() const override;

    queue<ActionInfo*> moveActions(Position _toPos) override;
};

#endif // ROOK_H
