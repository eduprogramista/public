#include "King.h"
#include "Global.h"
#include "Rook.h"
#include <cstdlib>

King::King(int _x, int _y, Color _color)
    : Chessman(_x, _y, _color, 'k')
{

}

King* King::clone() const
{
    return new King(*this);
}

queue<ActionInfo*> King::moveActions(Position _toPos)
{
    queue<ActionInfo*> actions;
    int dx = _toPos.x - pos.x;
    int dy = _toPos.y - pos.y;
    Chessman* attacked = gameBoard->getChessman(_toPos);
    int rx = (color == Color::BLACK ? 0 : Global::n - 1);
    int ry = (dy > 0 ? Global::n - 1 : 0);
    Rook* rook = dynamic_cast<Rook*>(gameBoard->getChessman(Position(rx, ry)));

    bool move = (abs(dx) == 1 && abs(dy) <= 1) || (abs(dx) <= 1 && abs(dy) == 1);
    bool moveBy1 = move && !attacked;
    bool attack = move && attacked && attacked->getColor() != color;
    bool castlingMove = dx == 0 && abs(dy) == 2 && !attacked && firstMove && rook && rook->isFirstMove();

    if(!moveBy1 && !attack && !castlingMove)
        return actions;

    if(abs(dy) > 1)
    {
        int sy = dy / abs(dy);
        int i = pos.x;
        int j = pos.y + sy;
        while(j != _toPos.y)
        {
            if(gameBoard->getChessman(Position(i, j)))
                return actions;

            j += sy;
        }
    }

    if(attack)
        actions.push(RemoveActionInfoBuilder::createActionInfo(attacked));

    actions.push(MoveActionInfoBuilder::createActionInfo(this, pos, _toPos));

    if(castlingMove)
        actions.push(MoveActionInfoBuilder::createActionInfo(rook, rook->getPosition(), Position(rx, pos.y + dy / abs(dy))));

    return actions;
}
