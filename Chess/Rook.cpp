#include "Rook.h"
#include "ActionInfo.h"

Rook::Rook(int _x, int _y, Color _color)
    : Chessman(_x, _y, _color, 'r')
{

}

Rook* Rook::clone() const
{
    return new Rook(*this);
}

queue<ActionInfo*> Rook::moveActions(Position _toPos)
{
    queue<ActionInfo*> actions;
    int dx = _toPos.x - pos.x;
    int dy = _toPos.y - pos.y;
    Chessman* attacked = gameBoard->getChessman(_toPos);

    bool horizontal = dx == 0 && abs(dy) != 0;
    bool vertical = abs(dx) != 0 && dy == 0;
    bool horizontalMove = horizontal && !attacked;
    bool verticalMove = vertical && !attacked;
    bool horizontalAttack = horizontal && attacked && attacked->getColor() != color;
    bool verticalAttack = vertical && attacked && attacked->getColor() != color;

    if(!horizontalMove && !verticalMove && !horizontalAttack && !verticalAttack)
        return actions;

    if(horizontalMove || horizontalAttack)
    {
        int sy = dy / abs(dy);
        int i = pos.x;
        int j = pos.y + sy;

        while(j != _toPos.y)
        {
            if(gameBoard->getChessman(Position(i, j)))
                return actions;
            j += sy;
        }
    }
    else if(verticalMove || verticalAttack)
    {
        int sx = dx / abs(dx);
        int i = pos.x + sx;
        int j = pos.y;

        while(i != _toPos.x)
        {
            if(gameBoard->getChessman(Position(i, j)))
                return actions;
            i += sx;
        }
    }

    if(horizontalAttack || verticalAttack)
        actions.push(RemoveActionInfoBuilder::createActionInfo(attacked));

    actions.push(MoveActionInfoBuilder::createActionInfo(this, pos, _toPos));

    return actions;
}
