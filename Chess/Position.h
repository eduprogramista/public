#ifndef POSITION_H
#define POSITION_H

struct Position
{
    Position(int _x = -1, int _y = -1);

    bool operator==(const Position& other);

    int x;
    int y;
};

#endif // POSITION_H
