#include "KingTest.h"
#include "GameBoard.h"
#include "King.h"
#include "ChessmanFactory.h"
#include <iostream>

KingTest::KingTest()
{
    testCase0();
    testCase1();
    testCase2();
    testCase3();
    testCase4();
    testCase5();
    testCase6();
    testCase7();
    testCase6();
    testCase7();
    testCase8();
    testCase9();
    testCase10();
    testCase11();
    testCase12();
    testCase13();
    testCase14();
    testCase15();
//    testCase16();
//    testCase17();
//    testCase18();
//    testCase19();
//    testCase20();
//    testCase21();
//    testCase22();
//    testCase23();
//    testCase24();
//    testCase25();
}

// standard moveActions

void KingTest::testCase0()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    King* king = blackFactory.createKing(3, 3);
    assert(king->moveActions(Position(3, 4)).empty() == false);
}

void KingTest::testCase1()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    King* king = blackFactory.createKing(3, 3);
    assert(king->moveActions(Position(4, 4)).empty() == false);
}

void KingTest::testCase2()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    King* king = blackFactory.createKing(3, 3);
    assert(king->moveActions(Position(4, 3)).empty() == false);
}

void KingTest::testCase3()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    King* king = blackFactory.createKing(3, 3);
    assert(king->moveActions(Position(4, 2)).empty() == false);
}

void KingTest::testCase4()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    King* king = blackFactory.createKing(3, 3);
    assert(king->moveActions(Position(3, 2)).empty() == false);
}

void KingTest::testCase5()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    King* king = blackFactory.createKing(3, 3);
    assert(king->moveActions(Position(2, 2)).empty() == false);
}

void KingTest::testCase6()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    King* king = blackFactory.createKing(3, 3);
    assert(king->moveActions(Position(2, 3)).empty() == false);
}

void KingTest::testCase7()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    King* king = blackFactory.createKing(3, 3);
    assert(king->moveActions(Position(2, 4)).empty() == false);
}

// standard moveActions occupied same color

void KingTest::testCase8()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    King* king = blackFactory.createKing(3, 3);
    Pawn* pawn = blackFactory.createPawn(3, 4);
    assert(king->moveActions(Position(3, 4)).empty() == true);
}

void KingTest::testCase9()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    King* king = blackFactory.createKing(3, 3);
    Pawn* pawn = blackFactory.createPawn(4, 4);
    assert(king->moveActions(Position(4, 4)).empty() == true);
}

void KingTest::testCase10()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    King* king = blackFactory.createKing(3, 3);
    Pawn* pawn = blackFactory.createPawn(4, 3);
    assert(king->moveActions(Position(4, 3)).empty() == true);
}

void KingTest::testCase11()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    King* king = blackFactory.createKing(3, 3);
    Pawn* pawn = blackFactory.createPawn(4, 2);
    assert(king->moveActions(Position(4, 2)).empty() == true);
}

void KingTest::testCase12()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    King* king = blackFactory.createKing(3, 3);
    Pawn* pawn = blackFactory.createPawn(3, 2);
    assert(king->moveActions(Position(3, 2)).empty() == true);
}

void KingTest::testCase13()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    King* king = blackFactory.createKing(3, 3);
    Pawn* pawn = blackFactory.createPawn(2, 2);
    assert(king->moveActions(Position(2, 2)).empty() == true);
}

void KingTest::testCase14()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    King* king = blackFactory.createKing(3, 3);
    Pawn* pawn = blackFactory.createPawn(2, 3);
    assert(king->moveActions(Position(2, 3)).empty() == true);
}

void KingTest::testCase15()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    King* king = blackFactory.createKing(3, 3);
    Pawn* pawn = blackFactory.createPawn(2, 4);
    assert(king->moveActions(Position(2, 4)).empty() == true);
}

// standard moveActions occupied different color


// incorrect moveActions


// castling


// incorrect castling, bacause not first king moveActions


// incorrect castling, bacause not first king moveActions


// incorrect castling, bacause not first king moveActions


// incorrect castling, bacause occupied

