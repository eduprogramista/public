#include "ChessWidget.h"
#include <QApplication>

#include "GameBoard.h"
#include "ChessmanFactory.h"
#include "RookTest.h"
#include "QueenTest.h"
#include "KingTest.h"

#include <iostream>

// klasa abstrakcyjna pionka
// klasa pionka
// klasa adaptera hetmana / innego piona
// klasa gry

// widget gry
// widget pionka

// pionek musi mieć dostęp do planszy, znać swoją pozycję

// pionek: bicie - po przekątnej lub przelotem, ruch do przodu o 1 lub 2
// goniec: bicie - po L, ruch po L
// laufer: bicie - po przekątnej, ruch po przekątnej
// wieza : bicie - po prostej, ruch po prostej, ROSZADA
// hetman: bicie - po prostej lub po przekątnej, ruch po prostej lub przekątnej
// król  : bicie - najbliższe pole, ruch najbliższe pole, ROSZADA
// AVANS PIONKA
// SZACH i MAT, PAT

// wzorce projektowe - dekorator, monostate ?, fabryka, builder,

/*void pawnTest()
{
    GameBoard board;
    ChessmanFactory bBuilder(&board, Color::BLACK);
    ChessmanFactory wBuilder(&board, Color::WHITE);

    Pawn* pawn0 = bBuilder.createPawn(1, 2);
    Chessman* chessman0 = pawn0;
    assert(chessman0 == pawn0);
    assert(pawn0->moveActions(4, 2) == false); // more than 2 step
    assert(pawn0->moveActions(0, 2) == false); // back
    assert(pawn0->moveActions(2, 2) == true);  // right moveActions

    Pawn* pawn1 = bBuilder.createPawn(3, 2);
    assert(pawn1->moveActions(3, 2) == false); // moveActions on occupied ceil

    Pawn* pawn2 = bBuilder.createPawn(1, 5);
    assert(pawn2->moveActions(3, 5) == true);
    assert(pawn2->moveActions(5, 5) == false);

    Pawn* pawn3 = bBuilder.createPawn(1, 6);
    Pawn* pawn4 = bBuilder.createPawn(3, 6);
    assert(pawn3->moveActions(3, 6) == false);

    assert(pawn3->moveActions(2, 7) == false);
    Pawn* pawn5 = wBuilder.createPawn(2, 5);
    assert(pawn3->moveActions(2, 5) == true);
}*/

/*void bishopTest()
{
    GameBoard board;
    ChessmanFactory bBuilder(&board, Color::BLACK);
    ChessmanFactory wBuilder(&board, Color::WHITE);

    Bishop* bishop0 = bBuilder.createBishop(3, 3);
    Bishop* bishop1 = bBuilder.createBishop(7, 7);
    Bishop* bishop2 = wBuilder.createBishop(6, 6);
    assert(bishop0->moveActions(4, 7) == false);
    assert(bishop0->moveActions(7, 7) == false);
    assert(bishop0->moveActions(4, 4) == true);
    assert(bishop0->moveActions(6, 6) == true);

    // white
}*/

/*void knightTest()
{
    GameBoard board;
    ChessmanFactory bBuilder(&board, Color::BLACK);
    ChessmanFactory wBuilder(&board, Color::WHITE);

    Knight* knight0 = bBuilder.createKnight(4, 4);
    assert(knight0->moveActions(5, 5) == false);
    assert(knight0->moveActions(3, 2) == true);
    Pawn* pawn0 = bBuilder.createPawn(4, 4);
    assert(knight0->moveActions(4, 4) == false);
    Pawn* pawn1 = wBuilder.createPawn(2, 0);
    assert(knight0->moveActions(2, 0) == true);
}*/

void rookTest()
{
//    Board board;

//    Rook rook0(board, 7, 1, Chessman::Color::WHITE);
//    assert(rook0.moveActions(6, 2) == false);
//    assert(rook0.moveActions(6, 1) == true);
//    assert(rook0.moveActions(7, 1) == true);
}

int main(int argc, char* argv[])
{
    //QApplication a(argc, argv);
//    ChessWidget w;
//    w.show();
    /*pawnTest();
    bishopTest();*/
    RookTest rookTest;
    QueenTest queenTest;
    KingTest kingTest;

    std::cout << "Tests passed :D" << std::endl;

    GameBoard gameBoard;

    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);
    Rook* blackRook0 = blackFactory.createRook(0, 0);
    Knight* blackKnight0 = blackFactory.createKnight(0, 1);
    Bishop* blackBishop0 = blackFactory.createBishop(0, 2);
    Queen* blackQueen0 = blackFactory.createQueen(0, 3);
    King* blackKing0 = blackFactory.createKing(0, 4);
    Bishop* blackBishop1 = blackFactory.createBishop(0, 5);
    Knight* blackKnight1 = blackFactory.createKnight(0, 6);
    Rook* blackRook1 = blackFactory.createRook(0, 7);
    for(int i = 0; i < 8; ++i)
        Pawn* blackPawn = blackFactory.createPawn(1, i);

    ChessmanFactory whiteFactory(&gameBoard, Color::WHITE);
    Rook* whiteRook0 = whiteFactory.createRook(7, 0);
    Knight* whiteKnight0 = whiteFactory.createKnight(7, 1);
    Bishop* whiteBishop0 = whiteFactory.createBishop(7, 2);
    Queen* whiteQueen0 = whiteFactory.createQueen(7, 3);
    King* whiteKing0 = whiteFactory.createKing(7, 4);
    Bishop* whiteBishop1 = whiteFactory.createBishop(7, 5);
    Knight* whiteKnight1 = whiteFactory.createKnight(7, 6);
    Rook* whiteRook1 = whiteFactory.createRook(7, 7);
    for(int i = 0; i < 8; ++i)
        Pawn* whitePawn = whiteFactory.createPawn(6, i);
    std::cout << gameBoard << std::endl;

    //return a.exec();
    return 0;
}
