#include "RookTest.h"
#include "GameBoard.h"
#include "Rook.h"
#include "ChessmanFactory.h"
#include <iostream>

RookTest::RookTest()
{
    testCase0();
    testCase1();
    testCase2();
    testCase3();
    testCase4();
    testCase5();
    testCase6();
    testCase7();
    testCase8();
    testCase9();
    testCase10();
    testCase11();
    testCase12();
    testCase13();
    testCase14();
    testCase15();
    testCase16();
    testCase17();
}

// straight

void RookTest::testCase0()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Rook* rook = blackFactory.createRook(3, 3);
    assert(rook->moveActions(Position(3, 5)).empty() == false);
}

void RookTest::testCase1()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Rook* rook = blackFactory.createRook(3, 3);
    assert(rook->moveActions(Position(Position(5, 3))).empty() == false);
}

void RookTest::testCase2()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Rook* rook = blackFactory.createRook(3, 3);
    assert(rook->moveActions(Position(1, 3)).empty() == false);
}

void RookTest::testCase3()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Rook* rook = blackFactory.createRook(3, 3);
    assert(rook->moveActions(Position(3, 1)).empty() == false);
}

// straight occupied

void RookTest::testCase4()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Rook* rook = blackFactory.createRook(3, 3);
    Pawn* pawn = blackFactory.createPawn(3, 5);
    assert(rook->moveActions(Position(3, 6)).empty() == true);
}

void RookTest::testCase5()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Rook* rook = blackFactory.createRook(3, 3);
    Pawn* pawn = blackFactory.createPawn(5, 3);
    assert(rook->moveActions(Position(6, 3)).empty() == true);
}

void RookTest::testCase6()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Rook* rook = blackFactory.createRook(3, 3);
    Pawn* pawn = blackFactory.createPawn(1, 3);
    assert(rook->moveActions(Position(0, 3)).empty() == true);
}

void RookTest::testCase7()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Rook* rook = blackFactory.createRook(3, 3);
    Pawn* pawn = blackFactory.createPawn(3, 1);
    assert(rook->moveActions(Position(3, 0)).empty() == true);
}

// straight attack same color

void RookTest::testCase8()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Rook* rook = blackFactory.createRook(3, 3);
    Pawn* pawn = blackFactory.createPawn(3, 6);
    assert(rook->moveActions(Position(3, 6)).empty() == true);
}

void RookTest::testCase9()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Rook* rook = blackFactory.createRook(3, 3);
    Pawn* pawn = blackFactory.createPawn(6, 3);
    assert(rook->moveActions(Position(6, 3)).empty() == true);
}

void RookTest::testCase10()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Rook* rook = blackFactory.createRook(3, 3);
    Pawn* pawn = blackFactory.createPawn(0, 3);
    assert(rook->moveActions(Position(0, 3)).empty() == true);
}

void RookTest::testCase11()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Rook* rook = blackFactory.createRook(3, 3);
    Pawn* pawn = blackFactory.createPawn(3, 0);
    assert(rook->moveActions(Position(3, 0)).empty() == true);
}

// straight attack different color

void RookTest::testCase12()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);
    ChessmanFactory whiteFactory(&gameBoard, Color::WHITE);

    Rook* rook = blackFactory.createRook(3, 3);
    Pawn* pawn = whiteFactory.createPawn(3, 6);
    assert(rook->moveActions(Position(3, 6)).empty() == false);
}

void RookTest::testCase13()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);
    ChessmanFactory whiteFactory(&gameBoard, Color::WHITE);

    Rook* rook = blackFactory.createRook(3, 3);
    Pawn* pawn = whiteFactory.createPawn(6, 3);
    assert(rook->moveActions(Position(6, 3)).empty() == false);
}

void RookTest::testCase14()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);
    ChessmanFactory whiteFactory(&gameBoard, Color::WHITE);

    Rook* rook = blackFactory.createRook(3, 3);
    Pawn* pawn = whiteFactory.createPawn(0, 3);
    assert(rook->moveActions(Position(0, 3)).empty() == false);
}

void RookTest::testCase15()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);
    ChessmanFactory whiteFactory(&gameBoard, Color::WHITE);

    Rook* rook = blackFactory.createRook(3, 3);
    Pawn* pawn = whiteFactory.createPawn(3, 0);
    assert(rook->moveActions(Position(3, 0)).empty() == false);
}

// incorrect moveActions

void RookTest::testCase16()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Rook* rook = blackFactory.createRook(3, 3);
    assert(rook->moveActions(Position(5, 6)).empty() == true);
}

void RookTest::testCase17()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;

    GameBoard gameBoard;
    ChessmanFactory blackFactory(&gameBoard, Color::BLACK);

    Rook* rook = blackFactory.createRook(3, 3);
    assert(rook->moveActions(Position(7, 7)).empty() == true);
}
