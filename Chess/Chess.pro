#-------------------------------------------------
#
# Project created by QtCreator 2018-05-02T14:34:18
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Chess
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    ChessWidget.cpp \
    main.cpp \
    Chessman.cpp \
    Pawn.cpp \
    Bishop.cpp \
    Knight.cpp \
    Rook.cpp \
    Queen.cpp \
    King.cpp \
    GameBoard.cpp \
    Position.cpp \
    QueenTest.cpp \
    RookTest.cpp \
    KingTest.cpp \
    PawnTest.cpp \
    KnightTest.cpp \
    BishopTest.cpp \
    ActionInfo.cpp \
    ActionInfoBuilder.cpp \
    ChessmanFactory.cpp

HEADERS += \
    ChessWidget.h \
    Chessman.h \
    Pawn.h \
    Bishop.h \
    Knight.h \
    Rook.h \
    Queen.h \
    King.h \
    Global.h \
    GameBoard.h \
    Position.h \
    Color.h \
    QueenTest.h \
    RookTest.h \
    KingTest.h \
    PawnTest.h \
    KnightTest.h \
    BishopTest.h \
    ActionInfo.h \
    ActionInfoBuilder.h \
    ChessmanFactory.h

RESOURCES += \
    resource.qrc
