#ifndef KNIGHT_H
#define KNIGHT_H

#include "Chessman.h"

class Knight : public Chessman
{
public:
    Knight(int _x, int _y, Color _color);
    Knight* clone() const override;

    queue<ActionInfo*> moveActions(Position _toPos) override;
};

#endif // KNIGHT_H
