#include "Position.h"

Position::Position(int _x, int _y) : x(_x), y(_y)
{

}

bool Position::operator==(const Position& other)
{
    return x == other.x && y == other.y;
}

