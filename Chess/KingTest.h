#ifndef KINGTEST_H
#define KINGTEST_H


class KingTest
{
public:
    KingTest();

    // standard move
    void testCase0();
    void testCase1();
    void testCase2();
    void testCase3();
    void testCase4();
    void testCase5();
    void testCase6();
    void testCase7();

    // standard move occupied same color
    void testCase8();
    void testCase9();
    void testCase10();
    void testCase11();
    void testCase12();
    void testCase13();
    void testCase14();
    void testCase15();

    // standard move occupied different color
    void testCase16();
    void testCase17();
    void testCase18();
    void testCase19();
    void testCase20();
    void testCase21();

    // incorrect move
    void testCase22();
    void testCase23();

    // castling
    void testCase24();
    void testCase25();

    // incorrect castling, bacause not first king move
    void testCase26();

    // incorrect castling, bacause not first rook move
    void testCase27();

    // incorrect castling, bacause not first king move
    void testCase28();

    // incorrect castling, bacause occupied
    void testCase29();
};

#endif // KINGTEST_H
