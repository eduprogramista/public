#ifndef ACTIONINFOBUILDER_H
#define ACTIONINFOBUILDER_H

class Chessman;
class ActionInfo;
struct Position;

struct RemoveActionInfoBuilder
{
    static ActionInfo* createActionInfo(Chessman* _chessman);
};

struct MoveActionInfoBuilder
{
    static ActionInfo* createActionInfo(Chessman* _chessman, const Position& _prevPosition, const Position& _nextPosition);
};

struct PromotionActionInfoBuilder
{
    static ActionInfo* createActionInfo(Chessman* _chessman);
};

#endif // ACTIONINFOBUILDER_H
