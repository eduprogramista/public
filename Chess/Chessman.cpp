#include "Chessman.h"
#include <cctype>

std::ostream& operator<<(std::ostream& out, const Chessman& chessman)
{
    return out << chessman.symbol;
}

Chessman::Chessman(int _x, int _y, Color _color, char _symbol)
    : symbol(_color == Color::WHITE ? _symbol : toupper(_symbol)), gameBoard(nullptr), pos(_x, _y), color(_color), firstMove(true)
{

}

void Chessman::setGameBoard(const GameBoard* _gameBoard)
{
    gameBoard = _gameBoard;
}

const GameBoard* Chessman::getGameBoard() const
{
    return gameBoard;
}

void Chessman::setPosition(const Position& _pos)
{
    pos = _pos;
}

const Position& Chessman::getPosition() const
{
    return pos;
}

Color Chessman::getColor() const
{
    return color;
}

void Chessman::setFirstMove(bool _firstMove)
{
    firstMove = _firstMove;
}

bool Chessman::isFirstMove() const
{
    return firstMove;
}
