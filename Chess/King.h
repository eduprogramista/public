#ifndef KING_H
#define KING_H

#include "Chessman.h"

class King : public Chessman
{
public:
    King(int _x, int _y, Color _color);
    King* clone() const override;

    queue<ActionInfo*> moveActions(Position _toPos) override;
};

#endif // KING_H
