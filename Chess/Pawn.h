#ifndef PAWN_H
#define PAWN_H

#include "Chessman.h"

class Pawn : public Chessman
{
public:
    Pawn(int _x, int _y, Color _color);
    Pawn* clone() const override;

    queue<ActionInfo*> moveActions(Position _toPos) override;
};

#endif // PAWN_H
