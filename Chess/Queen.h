#ifndef QUEEN_H
#define QUEEN_H

#include "Chessman.h"

class Queen : public Chessman
{
public:
    Queen(int _x, int _y, Color _color);
    Queen* clone() const override;

    queue<ActionInfo*> moveActions(Position _toPos) override;
};

#endif // QUEEN_H
