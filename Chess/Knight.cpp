#include "Knight.h"
#include <cstdlib>

Knight::Knight(int _x, int _y, Color _color)
    : Chessman(_x, _y, _color, 'j')
{

}

Knight* Knight::clone() const
{
    return new Knight(*this);
}

queue<ActionInfo*> Knight::moveActions(Position _toPos)
{
    queue<ActionInfo*> actions;
    int dx = _toPos.x - pos.x;
    int dy = _toPos.y - pos.y;
    Chessman* attacked = gameBoard->getChessman(_toPos);

    bool jump = abs(dx) == 2 && abs(dy) == 1 || abs(dx) == 1 && abs(dy) == 2;
    bool jumpMove = jump && !attacked;
    bool jumpAttack = jump && attacked && attacked->getColor() != color;

    if(!jumpMove && !jumpAttack)
        return actions;

    if(jumpAttack)
        actions.push(RemoveActionInfoBuilder::createActionInfo(attacked));

    actions.push(MoveActionInfoBuilder::createActionInfo(this, pos, _toPos));

    return actions;
}
