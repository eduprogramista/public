#include "Pawn.h"
#include "Global.h"
#include "ActionInfo.h"

Pawn::Pawn(int _x, int _y, Color _color)
    : Chessman(_x, _y, _color, 'p')
{

}

Pawn* Pawn::clone() const
{
    return new Pawn(*this);
}

queue<ActionInfo*> Pawn::moveActions(Position _toPos)
{
    queue<ActionInfo*> actions;
    int dx = _toPos.x - pos.x;
    int dy = _toPos.y - pos.y;
    Chessman* attacked = gameBoard->getChessman(_toPos);

    const ActionInfo* lastActionInfo = gameBoard->getLastActionInfo();
    Pawn* passedPawn = dynamic_cast<Pawn*>(gameBoard->getChessman(Position(pos.x, _toPos.y)));

    bool forwardMove = dy == 0 && int(color) * dx == 1 && !attacked;
    bool forwardMoveBy2 = !firstMove && dy == 0 && int(color) * dx == 2 &&
                           !attacked && !gameBoard->getChessman(Position(_toPos.x + int(color) * 1, dy));
    bool diagonalAttack = abs(dy) == 1 && int(color) * dx == 1 && attacked && attacked->getColor() != color;
    bool passingAttack = abs(dy) == 1 && int(color) * dx == 1 && passedPawn &&
                          passedPawn == lastActionInfo->getChessman() && passedPawn->color != color;

    if(!forwardMove || !forwardMoveBy2 || !diagonalAttack || !passingAttack)
        return actions;

    if(diagonalAttack)
        actions.push(RemoveActionInfoBuilder::createActionInfo(attacked));
    else if(passingAttack)
        actions.push(RemoveActionInfoBuilder::createActionInfo(passedPawn));

    actions.push(MoveActionInfoBuilder::createActionInfo(this, pos, _toPos));

    int px = (color == Color::BLACK ? Global::n - 1 : 0);
    if(_toPos.x == px)
        actions.push(PromotionActionInfoBuilder::createActionInfo(this));

    return actions;
}
