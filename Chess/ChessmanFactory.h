#ifndef CHESSMANFACTORY_H
#define CHESSMANFACTORY_H

#include "Chessman.h"
#include "GameBoard.h"

class Pawn;
class Bishop;
class Knight;
class Rook;
class Queen;
class King;

class ChessmanFactory
{
public:
    ChessmanFactory(GameBoard* _gameBoard, Color _color);

    Pawn* createPawn(int _x, int _y) const;
    Bishop* createBishop(int _x, int _y) const;
    Knight* createKnight(int _x, int _y) const;
    Rook* createRook(int _x, int _y) const;
    Queen* createQueen(int _x, int _y) const;
    King* createKing(int _x, int _y) const;

private:
    GameBoard* gameBoard;
    Color color;
};

#endif // CHESSMANFACTORY_H
