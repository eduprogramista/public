#include "ChessmanFactory.h"
#include "Pawn.h"
#include "Bishop.h"
#include "Knight.h"
#include "Rook.h"
#include "Queen.h"
#include "King.h"

ChessmanFactory::ChessmanFactory(GameBoard* _gameBoard, Color _color)
    : gameBoard(_gameBoard), color(_color)
{

}

Pawn* ChessmanFactory::createPawn(int _x, int _y) const
{
    Pawn* pawn = new Pawn(_x, _y, color);
    gameBoard->addChessman(pawn);
    return pawn;
}

Bishop* ChessmanFactory::createBishop(int _x, int _y) const
{
    Bishop* bishop = new Bishop(_x, _y, color);
    gameBoard->addChessman(bishop);
    return bishop;
}

Knight* ChessmanFactory::createKnight(int _x, int _y) const
{
    Knight* knight = new Knight(_x, _y, color);
    gameBoard->addChessman(knight);
    return knight;
}

Rook* ChessmanFactory::createRook(int _x, int _y) const
{
    Rook* rook = new Rook(_x, _y, color);
    gameBoard->addChessman(rook);
    return rook;
}

Queen* ChessmanFactory::createQueen(int _x, int _y) const
{
    Queen* queen = new Queen(_x, _y, color);
    gameBoard->addChessman(queen);
    return queen;
}

King* ChessmanFactory::createKing(int _x, int _y) const
{
    King* king = new King(_x, _y, color);
    gameBoard->addChessman(king);
    return king;
}


