#include "ActionInfo.h"

ActionInfo::ActionInfo(ActionType _actionType, Chessman* _chessman, const Position& _prevPosition, const Position& _nextPosition)
    : actionType(_actionType), chessman(_chessman), prevPosition(_prevPosition), nextPosition(_nextPosition)
{

}

ActionType ActionInfo::getActionType() const
{
    return actionType;
}

Chessman* ActionInfo::getChessman() const
{
    return chessman;
}

Position ActionInfo::getPrevPosition() const
{
    return prevPosition;
}

Position ActionInfo::getNextPosition() const
{
    return nextPosition;
}
