#include "GameBoard.h"
#include "Chessman.h"
#include "ChessmanFactory.h"
#include "Queen.h"
#include <ostream>

using namespace std;

ostream& operator<<(ostream& out, const GameBoard& gameBoard)
{
    const GameBoard::Board& board = gameBoard.board;
    out << "  ";
    for(int i = 0, n = Global::n; i < n; ++i)
        out << "  " << i << " ";
    out << "\n  +---+---+---+---+---+---+---+---+\n";
    for(int i = 0, n = Global::n; i < n; ++i)
    {
        out << i << " |";
        for(int j = 0; j < Global::n; ++j)
        {
            if(board[i][j])
                out << " " << *board[i][j] << " |";
            else if((i + j) % 2 == 0)
                out << "   |";
            else
                out << "###|";
        }
        out << "\n  +---+---+---+---+---+---+---+---+\n";
    }

    return out;
}

GameBoard::GameBoard() : check(false), checkmate(false), lastActionInfo(nullptr)
{
    removeAllChessmen();
}

GameBoard::GameBoard(const GameBoard& other) : check(other.check), checkmate(other.checkmate), lastActionInfo(other.lastActionInfo)
{
    for(int i = 0; i < Global::n; ++i)
        for(int j = 0; j < Global::n; ++j)
            board[i][j] = other.board[i][j]->clone();
}

GameBoard::~GameBoard()
{
    removeAllChessmen();
}

bool GameBoard::moveChessman(const Position& _fromPos, const Position& _toPos)
{
    queue<ActionInfo*> actions = board[_fromPos.x][_fromPos.y]->moveActions(_toPos);

    if(actions.empty())
        return false;

    while(!actions.empty())
    {
        ActionInfo* actionInfo = actions.front();
        ActionType actionType = actionInfo->getActionType();

        if(actionType == ActionType::REMOVE)
        {
            unsetChessman(actionInfo->getChessman());
        }
        else if(actionType == ActionType::MOVE)
        {
            setChessman(actionInfo->getNextPosition(), actionInfo->getChessman());
            actionInfo->getChessman()->setFirstMove(false);
        }
        else if(actionType == ActionType::PROMOTION)
        {
            Chessman* chessman = actionInfo->getChessman();
            const Position& pos = actionInfo->getNextPosition();
            ChessmanFactory factory(this, chessman->getColor());
            Queen* queen = factory.createQueen(pos.x, pos.y);
            queen->setFirstMove(false);
            setChessman(pos, queen);
        }

        delete actionInfo;
        actions.pop();
    }

    return true;
}

Chessman* GameBoard::getChessman(const Position& _pos) const
{
    return board[_pos.x][_pos.y];
}

void GameBoard::setChessman(const Position& _pos, Chessman* _chessman)
{
    board[_pos.x][_pos.y] = _chessman;
    _chessman->setPosition(_pos);
}

void GameBoard::unsetChessman(Chessman* _chessman)
{
    const Position& _pos = _chessman->getPosition();
    board[_pos.x][_pos.y] = nullptr;
}

void GameBoard::unsetAllChessmen()
{
    for(int i = 0; i < Global::n; ++i)
        for(int j = 0; j < Global::n; ++j)
            board[i][j] = nullptr;
    delete lastActionInfo;
    lastActionInfo = nullptr;
}

bool GameBoard::isCheck() const
{
    return check;
}

bool GameBoard::isCheckmate() const
{
    return checkmate;
}

const ActionInfo* GameBoard::getLastActionInfo() const
{
    return lastActionInfo;
}

void GameBoard::setLastActionInfo(const ActionInfo& _lastActionInfo)
{
    lastActionInfo = new ActionInfo(_lastActionInfo);
}

void GameBoard::undoLastAction()
{

}

void GameBoard::addChessman(Chessman* _chessman)
{
    chessmen.insert(_chessman);
    setChessman(_chessman->getPosition(), _chessman);
    _chessman->setGameBoard(this);
}

void GameBoard::removeChessman(Chessman* _chessman)
{
    unsetChessman(_chessman);
    chessmen.erase(_chessman);
}

void GameBoard::removeAllChessmen()
{
    unsetAllChessmen();
    unordered_set<Chessman*>::iterator iter = chessmen.begin();
    while(iter != chessmen.end())
        delete *iter++;
    chessmen.clear();
}
