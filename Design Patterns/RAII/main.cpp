#include <iostream>
#include <thread>
#include "ThreadRAII.h"

using namespace std;

int main()
{
    ThreadRAII t([](){
        long long i = 0;
        while(true)
        {
            cout << "func: " << i << "\n";
            i++;
            this_thread::sleep_for(chrono::milliseconds(500));
        }
    });

    return 0;
}
