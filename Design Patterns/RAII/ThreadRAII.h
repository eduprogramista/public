#ifndef THREADRAII_H
#define THREADRAII_H

#include <thread>

class ThreadRAII
{
public:
    template<typename Callable, typename... Args>
    explicit ThreadRAII(Callable&& f, Args&&... args);
    ~ThreadRAII();

private:
    std::thread t;
};

template<typename Callable, typename... Args>
ThreadRAII::ThreadRAII(Callable&& f, Args&&... args)
{
    t = std::thread(std::forward<Callable>(f), std::forward<Args>(args)...);
}

#endif // THREADRAII_H
