#include <iostream>
#include "Cloneable.h"

using namespace std;

class Integer : public Cloneable<Integer>
{
    friend std::ostream& operator<<(std::ostream& out, const Integer& integer);
public:
    Integer(int _value = 0);

    void setValue(int _value);
    int getValue() const;

private:
    int value;
};

std::ostream& operator<<(std::ostream& out, const Integer& integer)
{
    return out << integer.getValue();
}

Integer::Integer(int _value) : value(_value)
{

}

void Integer::setValue(int _value)
{
    value = _value;
}

int Integer::getValue() const
{
    return value;
}

int main()
{
    const Cloneable<Integer>& prototype = Integer(7);
    shared_ptr<Integer> i1(prototype.clone());
    cout << *i1 << endl;
    shared_ptr<Integer> i2(prototype.clone());
    cout << *i2 << endl;

    return 0;
}
