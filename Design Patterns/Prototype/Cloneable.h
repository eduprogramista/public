#ifndef CLONEABLE_H
#define CLONEABLE_H

template <class T>
class Cloneable
{
public:
    virtual ~Cloneable() = default;
    T* clone() const;
};

template <class T>
T* Cloneable<T>::clone() const
{
    return new T(dynamic_cast<const T&>(*this));
}

#endif // CLONEABLE_H
