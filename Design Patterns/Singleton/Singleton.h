#ifndef SINGLETON_H
#define SINGLETON_H

#include <memory>

class Singleton
{
    friend std::shared_ptr<Singleton> std::make_shared<Singleton>();
public:
    Singleton();

public:
    static std::shared_ptr<Singleton> getInstance();

    int getValue() const;
    void setValue(int _value);

private:
    int value;
};

#endif // SINGLETON_H
