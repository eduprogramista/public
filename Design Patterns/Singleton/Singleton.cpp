#include "Singleton.h"

Singleton::Singleton() : value(0)
{

}

std::shared_ptr<Singleton> Singleton::getInstance()
{
    static std::shared_ptr<Singleton> ptr(nullptr);
    if(!ptr) ptr = std::make_shared<Singleton>();

    return ptr;
}

int Singleton::getValue() const
{
    return value;
}

void Singleton::setValue(int _value)
{
    value = _value;
}
