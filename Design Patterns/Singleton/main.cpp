#include <iostream>
#include "Singleton.h"

using namespace std;

int main(void)
{

    Singleton::getInstance()->setValue(13);
    int value = Singleton::getInstance()->getValue();
    cout << "value: " << value << endl;

    return 0;
}