#include <iostream>
#include "Model.h"
#include "View.h"

using namespace std;

int main(void)
{
    View view;
    Model model(7);
    model.attach(&view);
    model.setValue(11);
    model.setValue(13);

    return 0;
}
