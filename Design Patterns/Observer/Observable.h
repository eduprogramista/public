#ifndef OBSERVABLE_H
#define OBSERVABLE_H

#include <vector>

class Observer;

class Observable
{
public:
    virtual ~Observable() = default;

    void attach(Observer* _observer);
    void detach(Observer* _observer);
    void detachAll();
    void notifyAll();

private:
    std::vector<Observer*> observers;
};

#endif // OBSERVABLE_H
