#include "Observable.h"
#include "Observer.h"
#include <algorithm>

void Observable::attach(Observer* _observer)
{
    observers.push_back(_observer);
}

void Observable::detach(Observer* _observer)
{
    observers.erase(std::remove(observers.begin(), observers.end(), _observer), observers.end());
}

void Observable::detachAll()
{
    observers.clear();
}

void Observable::notifyAll()
{
    for(auto& e : observers)
        e->notify(this);
}
