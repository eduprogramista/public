#ifndef OBSERVER_H
#define OBSERVER_H

#include <type_traits>

class Observable;

class Observer
{
public:
    virtual ~Observer() = default;

    virtual void notify(Observable* _observable)  = 0;
};

#endif // OBSERVER_H
