#ifndef VIEW_H
#define VIEW_H

#include "Observer.h"

class Observable;

class View : public Observer
{
public:
    void notify(Observable* _observable) override;
};

#endif // VIEW_H
