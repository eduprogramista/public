#include "View.h"
#include "Model.h"
#include <iostream>

void View::notify(Observable *_observable)
{
    Model* model = dynamic_cast<Model*>(_observable);
    if(model)
        std::cout << "view: " << model->getValue() << std::endl;
}
