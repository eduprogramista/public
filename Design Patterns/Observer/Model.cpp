#include "Model.h"

Model::Model(int _value) : value(_value)
{

}

int Model::getValue() const
{
    return value;
}

void Model::setValue(int _value)
{
    value = _value;
    notifyAll();
}
