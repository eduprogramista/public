#ifndef MODEL_H
#define MODEL_H

#include "Observable.h"

class Model : public Observable
{
public:
    Model(int _value);

    int getValue() const;
    void setValue(int _value);

private:
    int value;
};

#endif // MODEL_H
