#include <iostream>
#include "BSTree.h"

using namespace std;

int main()
{
    BSTree<int> tree;
    tree.insert(50);
    tree.insert(30);
    tree.insert(20);
    tree.insert(40);
    tree.insert(70);
    tree.insert(60);
    tree.insert(80);
    tree.insert(90);
    tree.insert(35);
    tree.insert(100);
    cout << tree;
    cout << "preorder: " << tree.toPreStr() << endl;
    cout << "inorder: " << tree.toInStr() << endl;
    cout << "postorder: " << tree.toPostStr() << endl;
    cout << "=============================================\n";
    tree.remove(20);
    tree.remove(30);
    tree.remove(50);
    tree.insert(10);
    tree.insert(90);
    tree.insert(30);
    cout << tree;
    return 0;
}
