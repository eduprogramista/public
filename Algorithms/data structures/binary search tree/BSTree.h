#ifndef BSTREE_H
#define BSTREE_H

#include <ostream>
#include <sstream>

template <typename T>
class BSTree final
{
    struct Node
    {
        Node(const T& _key) : key(_key), left(nullptr), right(nullptr) { }

        T key;
        Node* left;
        Node* right;
    };

    friend std::ostream& operator<<(std::ostream& out, const BSTree<T>& tree)
    {
        static std::function<void(std::ostream&, const Node*, int)> lambda =
                [](std::ostream& out, const Node* node, int k) 
		{
            if(node) 
			{
                lambda(out, node->right, k + 1);
                out << std::string(2 * k, ' ') << "" << node->key << "\n";
                lambda(out, node->left, k + 1);
            }
        };

        lambda(out, tree.root, 0);
        return out;
    }

public:
    BSTree();
    ~BSTree();

    bool insert(const T& key);
    bool remove(const T& key);

    void clear();
    void size() const noexcept;

    std::string toPreStr() const;
    std::string toInStr() const;
    std::string toPostStr() const;

private:
    Node* root;
    int n;
};

template <typename T>
BSTree<T>::BSTree() : root(nullptr), n(0)
{

}

template <typename T>
BSTree<T>::~BSTree()
{
    clear();
}

template <typename T>
bool BSTree<T>::insert(const T& key)
{
    static std::function<Node*(const T&, Node*, bool*)> lambda =
            [](const T& key, Node* node, bool* flag_ptr)->Node*
    {
        if(!node)
        {
            if(flag_ptr)
                *flag_ptr = true;
            return new Node(key);
        }

        if(node->key == key) return nullptr;

        if(node->key > key)
            node->left = lambda(key, node->left, flag_ptr);
        else if(node->key < key)
            node->right = lambda(key, node->right, flag_ptr);

        return node;
    };

    bool flag = false;
    root = lambda(key, root, &flag);
    if(flag)
        ++n;

    return flag;
}

template <typename T>
bool BSTree<T>::remove(const T& key)
{
    static std::function<Node*(Node*)> _findMin_ = [](BSTree<T>::Node* node)->Node*
    {
        while(node->left)
            node = node->left;
        return node;
    };

    static std::function<Node*(const T&, Node*, bool*)> _remove_ =
            [](const T& key, Node* node, bool* flag_ptr)->Node*
    {
        if(!node) return nullptr;

        if(node->key == key)
        {
            if(!node->left && !node->right)
            {
                delete node;
                node = nullptr;
            }
            else if(node->left && !node->right)
            {
                Node* tmp = node->left;
                delete node;
                node = tmp;
            }
            else if(!node->left && node->right)
            {
                Node* tmp = node->right;
                delete node;
                node = tmp;
            }
            else
            {
                Node* tmp = _findMin_(node->right);
                node->key = tmp->key;
                bool b;
                node->right = _remove_(tmp->key, node->right, &b);
            }

            if(flag_ptr)
                *flag_ptr = true;
        }
        else
        {
            if(node->key > key)
                node->left = _remove_(key, node->left, flag_ptr);
            else if(node->key < key)
                node->right = _remove_(key, node->right, flag_ptr);
        }

        return node;
    };

    bool flag = false;
    root = _remove_(key, root, &flag);
    if(flag)
        --n;

    return flag;
}

template <typename T>
void BSTree<T>::clear()
{
    static std::function<void(Node*)> _clear_ = [](Node* node)
    {
        if(node)
        {
            _clear_(node->left);
            _clear_(node->right);
        }

        delete node;
    };

    n = 0;
    _clear_(root);
    root = nullptr;
}

template <typename T>
void BSTree<T>::size() const noexcept
{
    return n;
}

template<typename T>
std::string BSTree<T>::toPreStr() const
{
    static std::function<void(std::ostream&, const Node* node)> _toPreStr_ =
            [](std::ostream& out, const Node* node)
    {
        if(!node) return;

        out << node->key << ", ";
        _toPreStr_(out, node->left);
        _toPreStr_(out, node->right);
    };

    std::stringstream sin;
    _toPreStr_(sin, root);
    return sin.str();
}

template<typename T>
std::string BSTree<T>::toInStr() const
{
    static std::function<void(std::ostream&, const Node* node)> _toInStr_ =
            [](std::ostream& out, const Node* node)
    {
        if(!node) return;

        _toInStr_(out, node->left);
        out << node->key << ", ";
        _toInStr_(out, node->right);
    };

    std::stringstream sin;
    _toInStr_(sin, root);
    return sin.str();
}

template<typename T>
std::string BSTree<T>::toPostStr() const
{
    static std::function<void(std::ostream&, const Node* node)> _toPostStr_ =
            [](std::ostream& out, const Node* node)
    {
        if(!node) return;

        _toPostStr_(out, node->left);
        _toPostStr_(out, node->right);
        out << node->key << ", ";
    };

    std::stringstream sin;
    _toPostStr_(sin, root);
    return sin.str();
}

#endif // BSTREE_H
