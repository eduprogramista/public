#ifndef DOUBLELINKEDLIST_H
#define DOUBLELINKEDLIST_H

#include <ostream>
#include <stdexcept>

template <typename T>
class DoubleLinkedList
{
    struct Node
    {
        T value;
        Node* next;
        Node* prev;
    };

    friend std::ostream& operator<<(std::ostream& out, const DoubleLinkedList<T>& list)
    {
        out << "forward: ";
        for(DoubleLinkedList<T>::Node* tmp = list.head; tmp != nullptr; tmp = tmp->next)
            out << tmp->value << " -> ";
        out << "NULL\n";

        out << "backward: ";
        for(DoubleLinkedList<T>::Node* tmp = list.tail; tmp != nullptr; tmp = tmp->prev)
            out << tmp->value << " -> ";
        out << "NULL";

        return out;
    }

public:
    DoubleLinkedList();
    virtual ~DoubleLinkedList();

    T& operator[](int pos);
    const T& operator[](int pos) const;

    void append(const T& value);
    void prepend(const T& value);
    bool insertAt(int pos, const T& value);
    bool removeAt(int pos);
    void reverse();
    void clear();
    int size() const noexcept;

private:
    Node* head;
    Node* tail;
    int n;

    inline Node* getNode(int pos);
    inline const Node* getNode(int pos) const;
};

template <typename T>
DoubleLinkedList<T>::DoubleLinkedList() : head(nullptr), tail(nullptr), n(0)
{

}

template <typename T>
DoubleLinkedList<T>::~DoubleLinkedList()
{
    clear();
}

template <typename T>
T& DoubleLinkedList<T>::operator[](int pos)
{
    if(pos >= n || pos < 0) throw std::out_of_range("No such element in list!");

    return getNode(pos)->value;
}

template <typename T>
const T& DoubleLinkedList<T>::operator[](int pos) const
{
    return const_cast<DoubleLinkedList<T>&>(*this)[pos];
}

template <typename T>
void DoubleLinkedList<T>::append(const T& value)
{
    Node* node = new Node;
    node->value = value;
    node->next = nullptr;
    node->prev = tail;
    if(!head)
        head = node;
    if(tail)
        tail->next = node;
    tail = node;
    ++n;
}

template <typename T>
void DoubleLinkedList<T>::prepend(const T& value)
{
    Node* node = new Node;
    node->value = value;
    node->next = head;
    node->prev = nullptr;
    if(head)
        head->prev = node;
    if(!tail)
        tail = node;
    head = node;
    ++n;
}

template <typename T>
bool DoubleLinkedList<T>::insertAt(int pos, const T& value)
{
    if(pos > n) return false;

    Node* node = new Node;
    node->value = value;
    node->next = nullptr;
    node->prev = nullptr;

    if(pos == 0)
    {
        node->next = head;
        if(head)
            head->prev = node;
        if(!tail)
            tail = node;
        head = node;
    }
    else if(pos == n)
    {
        node->prev = tail;
        tail->next = node;
        tail = node;
    }
    else
    {
        Node* tmp = getNode(pos - 1);
        node->prev = tmp;
        node->next = tmp->next;
        if(tmp->next)
            tmp->next->prev = node;
        tmp->next = node;
    }

    ++n;

    return true;
}

template <typename T>
bool DoubleLinkedList<T>::removeAt(int pos)
{
    if(pos >= n) return false;

    if(pos == 0)
    {
        Node* node = head;
        if(tail == head)
            tail = nullptr;
        head = head->next;
        if(head)
            head->prev = nullptr;
        delete node;
    }
    else
    {
        Node* tmp = getNode(pos - 1);
        Node* node = tmp->next;
        tmp->next = node->next;
        if(node->next)
            node->next->prev = tmp;
        if(tail == node)
            tail = tmp;
        delete node;
    }

    --n;

    return true;
}

template <typename T>
void DoubleLinkedList<T>::reverse()
{
    if(n <= 1) return;

    Node* rhead = head;
    Node* rtail = head;

    Node* tmp = head;
    tmp = tmp->next;
    rhead->next = nullptr;
    rhead->prev = nullptr;

    while(tmp)
    {
        Node* node = tmp;
        tmp = tmp->next;
        node->next = rhead;
        rhead->prev = node;
        rhead = node;
    }
    rhead->prev = nullptr;

    head = rhead;
    tail = rtail;
}

template <typename T>
void DoubleLinkedList<T>::clear()
{
    n = 0;

    Node* tmp;
    while(head)
    {
        tmp = head;
        head = head->next;
        delete tmp;
    }

    tail = nullptr;
}

template <typename T>
int DoubleLinkedList<T>::size() const noexcept
{
    return n;
}

template<typename T>
inline typename DoubleLinkedList<T>::Node* DoubleLinkedList<T>::getNode(int pos)
{
    if(pos >= n || pos < 0)
        return nullptr;

    if(pos == n - 1)
        return tail;

    Node* tmp;
    if(pos < n / 2)
    {
        tmp = head;
        for(int i = 0; i < pos; ++i)
            tmp = tmp->next;
    }
    else
    {
        tmp = tail;
        for(int i = n - 1; i > pos; i--)
            tmp = tmp->prev;
    }

    return tmp;
}

template<typename T>
inline const typename DoubleLinkedList<T>::Node* DoubleLinkedList<T>::getNode(int pos) const
{
    return const_cast<DoubleLinkedList<T>*>(this)->getNode(pos);
}

#endif // DOUBLELINKEDLIST_H
