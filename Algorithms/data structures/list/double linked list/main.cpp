#include <iostream>
#include "DoubleLinkedList.h"

using namespace std;

int main()
{
    DoubleLinkedList<int> list;
    list.insertAt(0, 5);
    list.prepend(1);
    list.append(7);
    list.append(5);
    list.prepend(2);
    list.insertAt(2, 8);
    list.append(9);
    list[1] = -5;
    cout << list << endl;
    list.removeAt(0);
    list.removeAt(3);
    cout << list << endl;
    list.reverse();
    cout << list << endl;

    return 0;
}
