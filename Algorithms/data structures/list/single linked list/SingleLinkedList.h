#ifndef SINGLELINKEDLIST_H
#define SINGLELINKEDLIST_H

#include <ostream>
#include <stdexcept>

template <typename T>
class SingleLinkedList
{
    struct Node
    {
        T value;
        Node* next;
    };

    friend std::ostream& operator<<(std::ostream& out, const SingleLinkedList<T>& list)
    {
        for(SingleLinkedList<T>::Node* tmp = list.head; tmp != nullptr; tmp = tmp->next)
            out << tmp->value << " -> ";
        out << "NULL";
        return out;
    }

public:
    SingleLinkedList();
    virtual ~SingleLinkedList();

    T& operator[](int pos);
    const T& operator[](int pos) const;

    void append(const T& value);
    void prepend(const T& value);
    bool insertAt(int pos, const T& value);
    bool removeAt(int pos);
    void reverse();
    void clear();
    int size() const noexcept;
	
private:
    Node* head;
    Node* tail;
    int n;
};

template <typename T>
SingleLinkedList<T>::SingleLinkedList() : head(nullptr), tail(nullptr), n(0)
{

}

template <typename T>
SingleLinkedList<T>::~SingleLinkedList()
{
    clear();
}

template <typename T>
T& SingleLinkedList<T>::operator[](int pos)
{
    if(pos >= n || pos < 0) throw std::out_of_range("No such element in list!");

    if(pos == n - 1)
        return tail->value;

    Node* tmp = head;
    for(int i = 0; i < pos; ++i)
        tmp = tmp->next;

    return tmp->value;
}

template <typename T>
const T& SingleLinkedList<T>::operator[](int pos) const
{
    if(pos >= n || pos < 0) throw std::out_of_range("No such element in list!");

    if(pos == n - 1)
        return tail->value;

    Node* tmp = head;
    for(int i = 0; i < pos; ++i)
        tmp = tmp->next;

    return tmp->value;
}

template <typename T>
void SingleLinkedList<T>::append(const T& value)
{
    Node* node = new Node;
    node->value = value;
    node->next = nullptr;
    if(!head)
        head = node;
    if(tail)
        tail->next = node;
    tail = node;
    ++n;
}

template <typename T>
void SingleLinkedList<T>::prepend(const T& value)
{
    Node* node = new Node;
    node->value = value;
    node->next = head;
    if(!tail)
        tail = node;
    head = node;
    ++n;
}

template <typename T>
bool SingleLinkedList<T>::insertAt(int pos, const T& value)
{
    if(pos > n) return false;

    Node* node = new Node;
    node->value = value;
    node->next = nullptr;

    if(pos == 0)
    {
        node->next = head;
        if(!tail)
            tail = node;
        head = node;
    }
    else if(pos == n)
    {
        tail->next = node;
        tail = node;
    }
    else
    {
        Node* tmp = head;
        for(int i = 0; i < pos - 1; ++i)
            tmp = tmp->next;

        node->next = tmp->next;
        tmp->next = node;
    }

    ++n;

    return true;
}

template <typename T>
bool SingleLinkedList<T>::removeAt(int pos)
{
    if(pos >= n) return false;

    if(pos == 0)
    {
        Node* node = head;
        if(tail == head)
            tail = nullptr;
        head = head->next;
        delete node;
    }
    else
    {
        Node* tmp = head;
        for(int i = 0; i < pos - 1; ++i)
            tmp = tmp->next;

        Node* node = tmp->next;
        tmp->next = node->next;
        if(tail == node)
            tail = tmp;
        delete node;
    }

    --n;

    return true;
}

template <typename T>
void SingleLinkedList<T>::reverse()
{
	if(n <= 1) return;
	
    Node* rhead = head;
    Node* rtail = head;
	
    Node* tmp = head;
    tmp = tmp->next;
    rhead->next = nullptr;

    while(tmp)
    {
        Node* node = tmp;
        tmp = tmp->next;
        node->next = rhead;
        rhead = node;
    }

    head = rhead;
    tail = rtail;
}

template <typename T>
void SingleLinkedList<T>::clear()
{
    n = 0;

    Node* tmp;
    while(head)
    {
        tmp = head;
        head = head->next;
        delete tmp;
    }

    tail = nullptr;
}

template <typename T>
int SingleLinkedList<T>::size() const noexcept
{
    return n;
}

#endif // SINGLELINKEDLIST_H
