#include <iostream>
#include "SingleLinkedList.h"

using namespace std;

int main()
{
    SingleLinkedList<int> list;
    list.insertAt(0, 5);
    list.prepend(1);
    list.append(7);
    list.append(5);
    list.prepend(2);
    list.removeAt(0);
    list.removeAt(3);
    list.append(9);
    list[1] = -5;
    cout << list << endl;
    list.reverse();
    cout << list << endl;

    return 0;
}
