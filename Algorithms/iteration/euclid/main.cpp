#include <iostream>

using namespace std;

int euclid(int a, int b)
{
    int tmp;

    while(b != 0)
    {
        tmp = b;
        b = a % b;
        a = tmp;
    }

    return a;
}

int main()
{
    cout << "=== Greatest common divisor - Euclid algorithm ===" << endl;

    int a, b;
    cout << "a: ";
    cin >> a;
    cout << "b: ";
    cin >> b;

    int gcd = euklides(a, b);
    cout << "gcd: " << gcd << endl;

    return 0;
}
