#include <iostream>

using namespace std;

int binarySearch(const int tab[], const int n, int value)
{
    int a = 0, b = n - 1;

    int pos = -1;
    while(a <= b && pos < 0)
    {
        int c = (a + b) / 2;

        if(tab[c] > value)
            b = c - 1;
        else if(tab[c] < value)
            a = c + 1;
        else
            pos = c;
    }

    return pos;
}

int main()
{
    cout << "=== binary search ==" << endl;

    const int n = 10;
    int tab[n] = { -7, -5, -1, 0, 3, 4, 5, 7, 8, 9 };
    for(int i = 0; i < n; ++i)
        cout << tab[i] << " ";
    cout << endl;

    cout << "value: ";
    int value;
    cin >> value;
    int pos = binarySearch(tab, n, value);
    cout << "pos: " << pos << endl;

    return 0;
}
