#include <iostream>

using namespace std;

int fibonacci (int n)
{
    int b = 1, a = 1, tmp;

    for(int i = 2; i < n; ++i)
    {
        tmp = a;
        a = b + a;
        b = tmp;
    }

    return a;
}

int main()
{
    cout << "=== Fibonacci sequence ===" << endl;

    cout << "n: ";
    int n;
    cin >> n;
    int a = fibonacci (n);
    cout << a << endl;

    return 0;
}
