#include <iostream>

using namespace std;

int euclid(int a, int b)
{
    if(b == 0) return a;

    return euclid(b, a % b);
}

int main()
{
    cout << "=== Greatest common divison - Euclid algorithm ===" << endl;

    int a, b;
    cout << "a: ";
    cin >> a;
    cout << "b: ";
    cin >> b;

    int gcd = euklides(a, b);
    cout << "greatest common divisor: " << gcd << endl;

    return 0;
}
