#include <iostream>

using namespace std;

long long expInt(int a, int b)
{
    if(b == 0) return 1;

    if(b == 1) return a;

    long long x = expInt(a, b / 2);

    return b % 2 == 0 ?  x * x : a * x * x;
}

int main()
{
    cout << "=== Fast integer exp ==" << endl;

    int a, b;
    cout << "a: ";
    cin >> a;
    cout << "b: ";
    cin >> b;

    int result = expInt(a, b);
    cout << "a ^ b = " << result << endl;

    return 0;
}
