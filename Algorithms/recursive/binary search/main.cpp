#include <iostream>

using namespace std;

int binarySearch(const int tab[], const int n, int value, int offset = 0)
{
    if(n == 0) return -1;

    int c = n / 2;

    if(tab[c] > value)
        return binarySearch(tab, c - 1, value, offset);

    if(tab[c] < value)
        return binarySearch(tab + c + 1, n - (c + 1), value, offset + c + 1);

    return c + offset;
}

int main()
{
    cout << "=== binary search ==" << endl;

    const int n = 10;
    int tab[n] = { -7, -5, -1, 0, 3, 4, 5, 7, 8, 9 };
    for(int i = 0; i < n; ++i)
        cout << tab[i] << " ";
    cout << endl;

    cout << "value: ";
    int value;
    cin >> value;
    int pos = binarySearch(tab, n, value);
    cout << "pos: " << pos << endl;

    return 0;
}
